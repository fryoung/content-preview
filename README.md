## POC: Contentful Entry Preview App

#### What?
Simple NextJS application that will accept a contentful entry slug or entry id and load that entry into the page. 

#### Why?
Contentful has the ability to associate a Preview URL with entries. The Preview URL is meant to allow content creators / editors to see how their content will look in your webapp before content is published to production.

This App is a simple demo of that capability.

#### Installation

```
npm install
```

1. Create a .contentfulrc.json file in the /api folder. Add the following keys to it:

```
{
    "accessToken": "<contentful-preview-token>",
    "activeSpaceId": "<contentful-space-id>",
    "activeEnvironmentId": "<contentful-environment-id>",
    "host": "preview.contentful.com"
  }
 
```
You can get these values in the Contentful Web app under Settings > API Keys (you need to be in master environment of your space to do this)


2. `npm run dev` -- fires up the app at http://localhost:3000


#### Requirements

To run this example using your own Contentful Space, You do the following:

** 1 )   Create a Content Model that matches the following schema:**


| Field Name | Data Type | Description |
|------|------|------|
| Title | Short Text | title of entry |
| Slug | Short Text | appearance: Slug |
| Abstract | Rich Text | Rich Text |
| Images | Asset[] | Media field allowing multiple assets  |



**2 ) Create A Content Preview and enable it for your Content Model**


You can do this by going to your 'master' environment, then selecting Settings > Content Preview from the menu. You can create mutiple content preview links and apply them to different Content Models. Your preview URL should look like the following:

```
http://localhost:3000/preview/{entry.fields.slug}
```

**3 ) Create a couple entries.** 
Click content the content preview button in the side panel of the entry editor in Contenful. Contentful entries should load into app.



#### Notes

The **Rich Text** data type returns a deeply nested array of objects from the API. To assist with rendering this, contentful has created a npm module
**@contentful/rich-text-react-renderer**
https://www.npmjs.com/package/@contentful/rich-text-react-renderer

##### Also See:
https://www.contentful.com/developers/docs/javascript/tutorials/rendering-contentful-rich-text-with-javascript/
https://www.contentful.com/developers/docs/concepts/rich-text/
https://www.contentful.com/developers/docs/tutorials/general/getting-started-with-rich-text-field-type/




#### Author(s)

- Frank Young
