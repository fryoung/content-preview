import React from 'react'
import { documentToReactComponents } from '@contentful/rich-text-react-renderer';
import styled from 'styled-components'

const NodeTypes = {
   document: 'div',
   paragraph: 'p',
   text: 'span'
}

const RichTextWrapper = styled.div`
    margin-top: 20px;
`

const RichTextConsumer = ({content, depth = 0}) => {
   return <RichTextWrapper> 
      {documentToReactComponents(content)}
    </RichTextWrapper>
}


export default RichTextConsumer;