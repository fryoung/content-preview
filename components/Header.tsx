import styled from 'styled-components'
import EntryStatus  from './EntryStatus'

const HeaderWrapper = styled.div`
    height: 70px;
	display: flex;
	justify-content: space-between;
    padding-left: 30px;
    align-items: center;
    background-color: rgb(44, 159, 208);
    color: #fff;
    .poc-title {
    	font-size: 20px;
    	font-family: Aspira, Helvetica, Helvetica Neue, Arial;
    	font-weight: 700;
    	text-transform: uppercase;
    	span {
    		color: #000
    	}
    }
`

export default ({entry, contentfulURL}) => {
	return (<HeaderWrapper>
		<div className='poc-title'>
			Demo: <span>Contentful Preview</span>
		</div>
		<EntryStatus 
			entry={entry} 
			contentfulURL={contentfulURL}
		/>		

	</HeaderWrapper>)
} 