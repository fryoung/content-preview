
import styled from 'styled-components'
import { getEntityStatus } from '../api/utils/contentful-cpa'
import { getContentfulEntryURL } from '../common/utils'

interface EntryStatusProps {
	entry: any,
	contentfulURL: string
}

const Status = {
	DRAFT: {
		label: "Draft",
		fg: "#fff",
		bg: "#ea9005"
	},
	CHANGED:  {
		label: "Pending Changes",
		fg: "#fff",
		bg: "#3072be"
	},
	PUBLISHED: {
		label: "No Changes",
		fg: "#fff",
		bg: "#0eb87f"
	},
	ARCHIVED: {
		label: "Archived",
		fg: "#fff",
	    bg: "#0eb87f"
	}
}

const Container = styled.div`
  font-size: 14px;
  padding: 5px;
`

const Link = styled.a`
	color: #fff;
	margin-left: 10px;
	margin-right: 30px;
`

const EntryStatus = ({entry, contentfulURL}: EntryStatusProps) => {
	
	// const status:string = getEntityStatus(entry);
	// const StatusBadge = styled.div`
	//     display: inline-block;
	// 	font-weight: bold;
	// 	text-transform: uppercase;
	// 	padding: 6px 20px;
	// 	border-radius: 4px;
	// 	background-color: ${Status[status].bg};
	// 	color: ${Status[status].fg};	
	// `
	// <StatusBadge>{Status[status].label}</StatusBadge>
    return (
		<Container>
		   <Link href={contentfulURL} target="_blank">Edit in Contentful</Link>
		</Container>
	)
}

export default EntryStatus;