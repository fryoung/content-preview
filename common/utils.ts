import {DEFAULT_LOCALE, contentfulBaseURL} from '../constants'

export function getContentfulEntryURL({space, environment, id}) {
    return `${contentfulBaseURL}/spaces/${space.sys.id}/environments/${environment.sys.id}/entries/${id}`
}

/**
 * Returns a shallower entry to simplify consumption by components
 * @param entry 
 * @param locale 
 */
export function unwrapEntry( entry, locale = DEFAULT_LOCALE ): any {
    if(!entry) {
        return console.error('unwrapEntry: ERROR', 'an entry object with fields is required');
    } 
    return Object.keys(entry.fields).reduce( (acc, key) => {
           if(key === 'images') {
                   const images = entry.fields[key];
             acc[key] = images.map( image => ({
                url: image.fields.file.url,
                size: image.fields.file.details.image
             }));
           }
           else {
               acc[key] = entry.fields[key];
           }
        return acc
    }, {});
 }
 