/** 
* API Calls
**/

import {ContentfulClientApi} from 'contentful'
import { getClient } from './client'
import {isSlug} from './helpers'

export interface ContentType {
    id: string
    name: string,
    fields: any[]
}

export interface Query {
    [key: string]: string | number
}

export async function getEntries(contentType: string, params: Query):Promise<any>{
    const client: ContentfulClientApi = await getClient();
    const query:Query = {'content_type': contentType, ...params};
    return await client.getEntries(query);
}

export async function getEntry(contentType: string, id: string):Promise<any>{

    const client: ContentfulClientApi = await getClient()
    let entry;
    let query:Query = {include: 3};

    try {
        if(isSlug(id)) {
            query = {...query, 'fields.slug[in]': id}            
            entry = await getEntries(contentType, query);
            if(entry){
                entry = entry.items[0];
            }
        }        
	    else {
            entry = await client.getEntry(id, query);
        } 

        return entry;
    }
    catch(err){
        console.error('failed getting getEntries with', JSON.stringify(query));
    	return {err}
    }
}