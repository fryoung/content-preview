import { Entry } from 'contentful'
import { ContentfulStatus } from '../../types'

/**
 * Contentful helper functions to detect and return entry state
 * @see https://www.contentful.com/developers/docs/tutorials/general/determine-entry-asset-state/
 * 
 * NOTE: This only works when using the Content Management API. 
 * The Content Preview Api does not return version or publishedVersion flag
 * 
 */

interface Entity {
    sys: Sys
}

/**
 * Sys object from Content Management API
 */
interface Sys {
    revision: number,
}

/*
* @param entity 
* @return boolean
*/
export function isDraft<T>(entity:Entity):boolean {
    return entity.sys.revision === 0;
}

/*
* @param entity 
* @return boolean
*/
export function isPublished<T>(entity:Entity):boolean  {
    return entity.sys.revision > 0
}


/**
 * @param entity 
 * @return enum ContentfulStatus
 */
export function getEntityStatus<T>(entity: Entity):string  {
    if(isDraft(entity)){
        return ContentfulStatus.DRAFT;
    }
    else if(isPublished(entity)){
        return ContentfulStatus.PUBLISHED;
    }
}