import { DEFAULT_LOCALE } from '../constants'

export function getFieldValue(fieldName, entry, locale = DEFAULT_LOCALE) {
    return entry.fields[fieldName].locale
}

/**
 * tries to determine whether or not string is a slug or a content id
 */
export function isSlug(str){
    if(str.includes('-') || isNaN(parseInt(str))){
      return true;
    }
    return false;
}