
const {createClient} = require('contentful');
let cache: any = {};

export async function init() {

    const { accessToken, activeSpaceId, activeEnvironmentId, host} = require('./.contentfulrc.json')
	
	const client = await createClient({ 
		accessToken, 
		space:activeSpaceId,
		environment: activeEnvironmentId,
		host
	});

	cache[host] = {client};
	console.info(`contentful client started using: ${host} space-id=${activeSpaceId}, environment-id = ${activeEnvironmentId}`);

	return {
		client
	}
}

export async function getClient(host) {
	if(cache[host]&& cache[host].client) { 
		return cache[host].client; 
	}	
	const { client } = await init();
	return client
}
