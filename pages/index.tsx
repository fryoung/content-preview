import styled from 'styled-components'

const Container = styled.div`
   padding: 80px;
   font-family: Helvetica, Arial;
   h1 {
      margin-bottom: 60px;
   }
   h4 {
       margin-top: 0px;
   }
`

const References = styled.div`
    margin-top: 60px;
    ul {
        list-style-type: none;
        padding: 0px;
        margin: 0px;
        li {
            padding: 10px;
            b {
                font-weight: 700;
                font-size: 11pt;
                display: block;
                margin-bottom: 5px;
            }
            a {
                color: rgb(44,159,208);
            }
            h4 {
                margin-top: 20px;
            }
        }
    }
`

const Description = styled.div`
   padding: 60px;
   background: #f5f5f5;
`

const Home = () => <Container>
    <h1>Contentful Entry Preview: POC</h1>
    <Description>
       <h4>What?</h4> 
       Demo NextJS application that loads contentful entries from a <i>slug</i> or entry <i>id</i> to demonstrate the Content Preview capabilities inside contentful.
       <p>       
       Entries will be previewed at http://localhost:3000/preview/[slug]
       </p>
    </Description>
    <References>
        <h3> References</h3>
        <ul>
            <li>
                <b>Set Up Content Preview:</b>
                <a href="https://www.contentful.com/r/knowledgebase/setup-content-preview/">https://www.contentful.com/r/knowledgebase/setup-content-preview/</a>
            </li>
            <li>
                <b>Default Preview Environments</b>
                <a href="https://www.contentful.com/developers/docs/tutorials/general/content-preview/">
                https://www.contentful.com/developers/docs/tutorials/general/content-preview/
                </a>
            </li>
            <li>
                <h4> Contentful Preview API</h4>
                <b>Entries</b>
                <a href="https://www.contentful.com/developers/docs/references/content-preview-api/#/reference/entries/entries-collection">
                    https://www.contentful.com/developers/docs/references/content-preview-api/#/reference/entries/entries-collection
                </a>
            </li>
            <li>
            <b>Relational Queries:</b>
                <a href="https://www.contentful.com/developers/docs/concepts/relational-queries/">
                https://www.contentful.com/developers/docs/concepts/relational-queries/
                </a>                
            </li>
        </ul>
    </References>
</Container>;

export default Home;