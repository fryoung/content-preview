import ImageGallery from 'react-image-gallery'
import { useRouter } from 'next/router';
import { getEntries, getEntry } from '../../api'
import { getContentfulEntryURL, unwrapEntry } from '../../common/utils'
import {defaultContentType} from '../../constants'

import RichTextConsumer from '../../components/RichTextConsumer'
import Header from '../../components/Header'
import '../../styles/index.css'
import 'react-image-gallery/styles/css/image-gallery-no-icon.css';

interface PreviewProps {
    slug: string,
    fields: {[key: string]: any},
    error: boolean | null,
	query,
	contentful: any
}

function EntryNotFound({payload}) {
	return <div className='error'> 
	    <h5>Could not find Contentful Entry using query below</h5>
	    <div className='message'>
	    	<pre>
	    	 { JSON.stringify(payload.query, null, 4) }
	    	</pre>
	    </div>
    </div>
}


const Preview = ({fields = {}, query, slug, error, contentful}: PreviewProps) => {
	return (
	    <div className="page-container">
			<div className="content-wrapper">
				<Header 
					entry={contentful.entry} 
					contentfulURL={contentful.url} 
				/>
	            <div className='content-container'>
	                {error && <EntryNotFound payload={{query, slug}} />}
					<h1>{fields.title}</h1>
					<div className="image-gallery-wrapper">
						<ImageGallery 
						    showThumbnails={false}
						    showPlayButton={false}
						    showBullets={true}
						    items={fields.images} 
						/>
					</div>
					<div className="abstract">
			            <RichTextConsumer content={fields.abstract} />
			        </div>
		        </div>			
			</div>
		</div>
	)
}

Preview.getInitialProps = async (context) => {
	
	const {err, req, query} = context;
	const { slug } = query;

	const contentType = defaultContentType; 

    let entry;
    let images;
	let error = null;

    try {
	   entry = await getEntry(contentType, slug);
	   const fields = unwrapEntry(entry);
	   // mapping image objects for the ImageGallery
	   if(Array.isArray(fields.images)) {
	       images = fields.images.map( ({url}) => ({ original: url }))
	   }
    }
    catch(err){
        console.error('Preview. Could not getEntry with', slug);
        error = true;
	}
	
	const contentful = {
		entry,		
		url: getContentfulEntryURL(entry.sys)
	}

    return {
		contentful,
    	slug,
    	error,
    	fields: {...entry.fields, images},
    	query,
        contentType
    }
};

export default Preview;