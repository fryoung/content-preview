 const ErrorPage = err => {
    return <div className='error'>
        <h1>oops. whatever is was you were looking for was not found</h1>
        <pre> { JSON.stringify(err, null, 2) } </pre>
    </div>
}

export default ErrorPage