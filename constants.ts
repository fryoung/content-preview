export const DEFAULT_LOCALE = 'en-US';


export const contentfulBaseURL = 'https://app.contentful.com';

// change this to whatever the id is of your ContentType
export const defaultContentType = 'page';	