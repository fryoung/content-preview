export enum ContentfulStatus {
    DRAFT = 'DRAFT',
    CHANGED = 'CHANGED',
    PUBLISHED = 'PUBLISHED',
    ARCHIVED = 'ARCHIVED',
}